import axios from 'axios';
import { logger } from '../submodules/shared-library/utils';

const secrets = require('@cloudreach/docker-secrets');

const instance = axios.create( {
    headers: {
        'Content-Type': 'application/json',
    }
});

export interface DomainDetails {
    domainName: string;
    sld: string;
    tld: string;
    purchasable: boolean;
    premium: boolean;
    purchasePrice: number;
    purchaseType: string;
    renewalPrice: number;
}

interface OrderRequest {
    domain: {
        domainName: string;
    };
    purchasePrice: number;
}

export class NameCom {

    public static async search(keyword: string): Promise<DomainDetails[]> {
        try {
            const {data} = await instance.post(`${NameCom.apiAuthorizedAddress}:search`, {keyword});
            logger.info(data);
            return data.results.filter(NameCom.filterDomains);
        } catch (error) {
            logger.error(error);
            return [];
        }
    }

    public static async getDetails(domain: string): Promise<DomainDetails> {
        try {
            const {data} = await instance.post(`${NameCom.apiAuthorizedAddress}:checkAvailability`, {domainNames: [domain]});
            const details = data.results[0];
            logger.info(details, `Domain details for ${domain}`);
            return details;
        } catch (error) {
            logger.error(error);
            return null;
        }
    }

    public static async registerDomain(domain: string): Promise<boolean> {
        try {
            await NameCom.validateDomainAvailability(domain);
            await NameCom.buyNewDomain(domain);
            await NameCom.configureRecords(domain);
            await NameCom.disableAutorenew(domain);
            return true;
        } catch (error) {
            logger.error(error);
            throw error;
        }
    }

    private static serverIp: string = process.env.SERVER_IP || '127.0.0.1';
    private static username: string = secrets.NAMECOM_USERNAME ? secrets.NAMECOM_USERNAME : process.env.NAMECOM_USERNAME;
    private static token: string = secrets.NAMECOM_TOKEN ? secrets.NAMECOM_TOKEN : process.env.NAMECOM_TOKEN;
    private static apiAddress: string = process.env.NAMECOM_API_ADDRESS || 'api.dev.name.com';
    private static apiAuthorizedAddress: string = `https://${NameCom.username}:${NameCom.token}@${NameCom.apiAddress}/v4/domains`;

    private static filterDomains = (domain: DomainDetails): boolean => {
        return domain.purchasable && domain.purchaseType === 'registration' && domain.purchasePrice && domain.renewalPrice && domain.tld !== 'club';
    }

    private static async validateDomainAvailability(domain: string): Promise<void> {
        const { data } = await instance.post(`${NameCom.apiAuthorizedAddress}:checkAvailability`, {
            domainNames: [domain]
        });

        if (!data.results[0].purchasable) {
            throw new Error('Domain not available');
        }
        if (data.results[0].purchaseType !== 'registration') {
            throw new Error('Domain already taken');
        }
    }

    private static async buyNewDomain(domain: string): Promise<void> {
        try {
            const orderRequest = await NameCom.prepareOrderRequest(domain);
            const {data} = await instance.post(NameCom.apiAuthorizedAddress, orderRequest);
            logger.info(data, `Order response for ${domain}`);
        } catch (error) {
            logger.error(error);
            throw error;
        }
    }

    private static async prepareOrderRequest(domain: string): Promise<OrderRequest> {
        const price = await NameCom.getDomainPrice(domain);
        return {
            domain: {
                domainName: domain
            },
            purchasePrice: price
        };
    }

    private static async getDomainPrice(domain: string): Promise<number> {
        const {data} = await instance.post(`${NameCom.apiAuthorizedAddress}:checkAvailability`, {domainNames: [domain] });
        return data.results[0].purchasePrice;
    }

    private static async configureRecords(domain: string): Promise<void> {
        try {
            const records = await NameCom.getDomainRecords(domain);

            logger.info(records, `Domain records for ${domain}`);

            const filteredARecords = records.filter((record: any) => record.type === 'A');

            logger.info(filteredARecords, `Filtered A records for ${domain}`);

            if (filteredARecords.length) {
                for (const record of filteredARecords) {
                    await NameCom.updateDomainRecordForEngrave(record, domain);
                }
            } else {
                await NameCom.createDomainRecordsForEngrave(domain);
            }

        } catch (error) {
            logger.error(error);
            throw new Error('Error while configuring records for domain');
        }
    }

    private static async disableAutorenew(domain: string): Promise<void> {
        try {
            const url = `${NameCom.apiAuthorizedAddress}/${domain}:disableAutorenew`;
            const {data} = await instance.post(url, {});
            logger.info(data, `Successfully disabled autorenew for ${domain}`);
        } catch (error) {
            logger.error(error);
            throw error;
        }
    }

    private static async createDomainRecordsForEngrave(domain: string): Promise<void> {
        const url = `${NameCom.apiAuthorizedAddress}/${domain}/records`;

        await instance.post(url, {
            host: '@',
            type: 'A',
            answer: NameCom.serverIp,
            ttl: 300
        });

        await instance.post(url, {
            host: 'www',
            type: 'A',
            answer: NameCom.serverIp,
            ttl: 300
        });

        await instance.post(url, {
            host: '*',
            type: 'A',
            answer: NameCom.serverIp,
            ttl: 300
        });
    }

    private static async updateDomainRecordForEngrave(record: any, domain: string): Promise<void> {
        const {data} = await instance.put(`${NameCom.apiAuthorizedAddress}/${domain}/records/${record.id}`, {
            host: record.host,
            type: 'A',
            answer: NameCom.serverIp,
            ttl: 300
        });
        logger.info(data, `Successfully updated domain record for ${domain}`);
    }

    private static async getDomainRecords(domain: string): Promise<string[]> {

        try {
            const { data } = await instance.get(`${NameCom.apiAuthorizedAddress}/${domain}/records`);
            if (data.results[0].records) {
                return data.results[0].records;
            }
            logger.info(`No records found for ${domain}`);
            return [];

        } catch (error) {
            logger.error(error, `Error while fetching records for ${domain}`);
            return [];
        }
    }

}
