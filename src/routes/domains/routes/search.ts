import { Request, Response } from 'express';
import { handleResponseError, generateHealthResponse } from '../../../submodules/shared-library';
import { NameCom } from '../../../modules/NameCom';
import { body } from 'express-validator/check';

const middleware: any[] =  [
    body('keyword').exists().isString().trim().customSanitizer((v) => v.toLowerCase())
];

async function handler(req: Request, res: Response): Promise<Response> {
    return handleResponseError(async () => {
        const { keyword } = req.body;
        return res.json(await NameCom.search(keyword));
    }, req, res);
}

export default {
    middleware,
    handler
}
