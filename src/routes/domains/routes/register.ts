import { Request, Response } from 'express';
import { handleResponseError, generateHealthResponse } from '../../../submodules/shared-library';
import { NameCom } from '../../../modules/NameCom';

const middleware: any[] =  [
];

async function handler(req: Request, res: Response) {
    return handleResponseError(async () => {
        const { domain } = req.body;
        const result = await NameCom.registerDomain(domain);
        return res.json(result);
    }, req, res);
}

export default {
    middleware,
    handler
}
