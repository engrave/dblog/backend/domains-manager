import { Request, Response } from 'express';
import { handleResponseError } from '../../../submodules/shared-library';
import { NameCom } from '../../../modules/NameCom';
import { body } from 'express-validator/check';

const middleware: any[] =  [
    body('domain').exists().trim().stripLow().customSanitizer((v) => v.toLowerCase()).isString().isURL().withMessage('Invalid domain name')
];

async function handler(req: Request, res: Response) {
    return handleResponseError(async () => {
        const { domain } = req.body;
        const result = await NameCom.getDetails(domain);
        return res.json(result);
    }, req, res);
}

export default {
    middleware,
    handler
}
