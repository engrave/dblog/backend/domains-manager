import * as express from 'express';
import details from './routes/details';
import search from './routes/search';
import register from './routes/register';

const domainsApi: express.Router = express.Router();
domainsApi.post('/search', search.middleware, search.handler);
domainsApi.post('/details', details.middleware, details.handler);
domainsApi.post('/register', register.middleware, register.handler);

export default domainsApi;
