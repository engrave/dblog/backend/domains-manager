declare global {
    namespace NodeJS {
        interface Global {
            __rootdir__: string;
        }
    }
}

global.__rootdir__ = '/app/dist';

import app from './app/app';
import { listenOnPort, logger } from './submodules/shared-library';

( async () => {

    try {
        listenOnPort(app, 3000);
    } catch (error) {
        logger.warn('Error encountered while starting the application: ', error.message);
        process.exit(1);
    }

})();
