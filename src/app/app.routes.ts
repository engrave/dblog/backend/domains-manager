import healthApi from '../routes/health/health.routes';
import domainsApi from '../routes/domains/domains.routes';
import * as httpCodes from 'http-status-codes';
import { Request, Response } from 'express';

const Sentry = require('@sentry/node');

function routes(app: any): void {
    app.use('/health', healthApi);
    app.use('/domains', domainsApi);

    app.use('*', (req: Request, res: Response) => {res.status(httpCodes.NOT_FOUND).json({message: 'Resource not found'}); });

    // The error handler must be before any other error middleware and after all controllers
    app.use(Sentry.Handlers.errorHandler());
}

export default routes;
