import * as express from 'express';
import routes from './app.routes';
import settings from './app.settings';
const Sentry = require('@sentry/node');
import { RewriteFrames, Debug } from '@sentry/integrations';

Sentry.init({
    dsn: 'https://c364e4991b4141efa671ad4ef086ed2b@sentry.engrave.dev/2',
    release: `${process.env.npm_package_name}@${process.env.npm_package_version}`,
    integrations: [
        new RewriteFrames({ root: global.__rootdir__ })
    ]});

const app = express();

settings(app);

routes(app);

export default app;
