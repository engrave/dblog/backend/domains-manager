const bodyParser = require('body-parser');
const cors = require('cors');
const Sentry = require('@sentry/node');

function settings(app: any) {
    // The request handler must be the first middleware on the app
    app.use(Sentry.Handlers.requestHandler());
    app.use(cors());
    app.use(bodyParser.urlencoded({ limit: '50mb', extended: false }));
    app.use(bodyParser.json({limit: '50mb', extended: true}));
}

export default settings;